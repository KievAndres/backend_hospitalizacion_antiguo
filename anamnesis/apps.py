from django.apps import AppConfig


class AnamnesisConfig(AppConfig):
    name = 'anamnesis'
