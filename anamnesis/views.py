from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.shortcuts import get_object_or_404
from django.db.models import Q

from .serializers import AnamnesisSerializer
from .models import Anamnesis

class ListaAnamnesis(APIView):

  def get(self, request, format=None):
    snippets = Anamnesis.objects.all()
    serializer = AnamnesisSerializer(snippets, many=True)
    return Response(serializer.data)

  def post(self, request, format=None):
    serializer = AnamnesisSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATE)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ElementoAnamnesis(APIView):

  def get_object(self, pk):
    try:
      return Anamnesis.objects.get(pk=pk)
    except Anamnesis.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = AnamnesisSerializer(snippet)
    return Response(serializer.data)