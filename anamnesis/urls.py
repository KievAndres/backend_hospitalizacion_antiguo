from django.urls import path
from .views import ListaAnamnesis, ElementoAnamnesis

urlpatterns = [
  path('', ListaAnamnesis.as_view()),
  path('<int:pk>/', ElementoAnamnesis.as_view())
]