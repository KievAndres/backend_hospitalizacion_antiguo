from django.urls import path
from .views import ListaAfiliado, ElementoAfiliado

urlpatterns = [
  path('', ListaAfiliado.as_view()),
  path('<int:pk>/', ElementoAfiliado.as_view())
]