from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.shortcuts import get_object_or_404
from django.db.models import Q

from .serializers import AfiliadoSerializer
from .models import Afiliado

class ListaAfiliado(APIView):

  def get(self, request, format=None):
    snippets = Afiliado.objects.all()
    serializer = AfiliadoSerializer(snippets, many=True)
    return Response(serializer.data)

  def post(self, request, format=None):
    serializer = AfiliadoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATE)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ElementoAfiliado(APIView):

  def get_object(self, pk):
    try:
      return Afiliado.objects.get(pk=pk)
    except Afiliado.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = AfiliadoSerializer(snippet)
    return Response(serializer.data)