from django.db import models

# Create your models here.

class Afiliado(models.Model):
  fecha_afiliacion = models.DateField()
  ci = models.IntegerField()
  a_paterno = models.CharField(max_length=50)
  a_materno = models.CharField(max_length=50)
  nombre1 = models.CharField(max_length=40)
  nombre2 = models.CharField(max_length=40)
  nombre3 = models.CharField(max_length=40)
  codigo_afiliacion = models.CharField(max_length=20)
  fecha_nacimiento = models.DateField()
  carrera = models.CharField(max_length=40)
  facultad = models.CharField(max_length=50)
  edad = models.IntegerField()
  sexo = models.CharField(max_length=40)
  vigencia_afiliacion = models.DateField()
  observaciones = models.TextField()

  def __str__(self):
    return self.nombre1
