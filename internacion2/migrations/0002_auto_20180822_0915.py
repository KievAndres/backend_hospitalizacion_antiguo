# Generated by Django 2.0.7 on 2018-08-22 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('internacion2', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotaInternacion2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('telefono', models.IntegerField()),
                ('padecimiento_actual', models.CharField(max_length=60)),
                ('diagnostico', models.CharField(max_length=70)),
                ('indicaciones', models.TextField()),
            ],
        ),
        migrations.DeleteModel(
            name='NotaInternacion',
        ),
    ]
