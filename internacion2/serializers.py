from rest_framework import serializers
from .models import NotaInternacion2

class NotaInternacionSerializer2(serializers.ModelSerializer):
    class Meta:
        model = NotaInternacion2
        fields = '__all__'