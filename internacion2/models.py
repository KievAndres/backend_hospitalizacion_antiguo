from django.db import models

# Create your models here.

class NotaInternacion2(models.Model):
    ci = models.IntegerField()
    fecha = models.DateField()
    telefono = models.IntegerField()
    padecimiento_actual = models.CharField(max_length=60)
    diagnostico = models.CharField(max_length=70)
    indicaciones = models.TextField()

    def __str__(self):
        return self.diagnostico