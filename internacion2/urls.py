from django.urls import path
from .views import ListaNotaInternacion2, ElementoNotaInternacion2

urlpatterns = [
    path('', ListaNotaInternacion2.as_view()),
    path('<int:pk>/', ElementoNotaInternacion2.as_view())
]