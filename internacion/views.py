from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.shortcuts import get_object_or_404
from django.db.models import Q

from .serializers import NotaInternacionSerializer
from .models import NotaInternacion

class ListaNotaInternacion(APIView):

    def get(self, request, format=None):
        snippets = NotaInternacion.objects.all()
        serializer = NotaInternacionSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = NotaInternacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ElementoNotaInternacion(APIView):

    def get_object(self, pk):
        try:
            return NotaInternacion.objects.get(pk=pk)
        except NotaInternacion.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = NotaInternacionSerializer(snippet)
        return Response(serializer.data)