from rest_framework import serializers
from .models import NotaInternacion

class NotaInternacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotaInternacion
        fields = '__all__'