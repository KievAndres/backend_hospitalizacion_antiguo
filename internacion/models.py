from django.db import models

# Create your models here.

class NotaInternacion(models.Model):
    nombre = models.CharField(max_length=50)
    carrera = models.CharField(max_length=60)
    facultad = models.CharField(max_length=60)
    telefono = models.IntegerField()
    fecha = models.DateField()
    descripcion = models.TextField()
    diagnostico = models.TextField()
    indicaciones = models.TextField()

    def __str__(self):
        return self.nombre