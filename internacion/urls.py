from django.urls import path
from .views import ListaNotaInternacion, ElementoNotaInternacion

urlpatterns = [
    path('', ListaNotaInternacion.as_view()),
    path('<int:pk>/', ElementoNotaInternacion.as_view())
]